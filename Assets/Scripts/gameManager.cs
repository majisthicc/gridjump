﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class gameManager : MonoBehaviour {
	public GameObject playerMeeple;
	// Use this for initialization
	void Start () {
		initGame();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	// Initial game function *Call once on game start
	void initGame () {
		initPlayer();
	}

	void placePlatform () {
		// generate platform -> calling randomPlatform()
		// place platform on x pos
	}

	void randomPlatform () {
		// random object and platform type
	}

	void initPlayer () {
		playerMeeple = GameObject.Instantiate(playerMeeple);
		playerMeeple = GameObject.FindWithTag("Player");
		// playerMeeple.Transform.translate(0f,1f,0f)
		// playerMeeple.position = Vector3(0f , 1f , 0f);
	}
}
